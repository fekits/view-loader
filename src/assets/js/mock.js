var $ = require('jquery/dist/jquery.min');

// 加载模板
// var view = require('../../npm/view-loader.cmd.min.js');
var view = require('../../../lib/procms-loader.js');

var McTinting = require('@fekit/mc-tinting');

var ScrollTo = require('@fekit/mc-scrollto');

// URL操作
let McURL = require('@fekit/mc-url/mc-url.cmd.min.js');
// 哈希操作
let hash = new McURL({ type: '#' });

// 代码高亮
let codeTinting = new McTinting({ theme: 'chrome' });

// 处理当前菜单高亮
let _active = (route) => {
  // 当前路由节点
  let activeRouteList = route.split('/');
  let links = document.getElementsByTagName('a');
  Object.keys(links).map((idx) => {
    let item = links[idx];
    $(item).removeClass('co-main');
    let routeMaps = '';
    activeRouteList.map((routeItem, index) => {
      routeMaps = routeMaps ? routeMaps + '/' + routeItem : routeItem;
      if (item.hash === '#route=' + routeMaps) {
        $(item).addClass('co-main');
      }
      // 处理最后以/结尾的hash
      if (index + 2 === activeRouteList.length) {
        if (item.hash === '#route=' + routeMaps + '/') {
          $(item).addClass('co-main');
        }
      }
    });
  });
};

// 加载路由
var load = function () {
  // 关闭导航菜单
  $('#mc-menu-switch').prop('checked', false);

  // 获取路由信息
  let sRoute = hash.get('route');
  if (!sRoute) {
    sRoute = 'home';
  }
  if (sRoute.match(/\w*\.html$/) === null) {
    sRoute += hash.get('id') ? '/detail.htm' : '/index.htm';
  }
  setTimeout(() => {
    // $('#framer').html(view('views/' + sRoute));
    _active(sRoute);
    // 刷新代码高亮
    codeTinting.refresh();

    // 滚动到指定位置
    let pos = hash.get('pos');
    if (pos && $('#' + pos)) {
      ScrollTo({
        to: {
          y: '#' + pos
        }
      });
    }
  }, 0);
};
view();
load();
window.onhashchange = function () {
  load();
};

// 全局设置字号
$('body').on('click', '.j_size', function () {
  $('html').attr('size', $(this).attr('size'));
});

// 回到顶部
$('body').on('click', '.j_totop', function () {
  // console.log(0);
  ScrollTo({
    to: { y: 0 }
  });
});

// 加载内容
$('body').on('click', '.j_load', function () {
  console.log($(this));
  let file = $(this).attr('load-file');
  let area = $(this).attr('load-area');
  if (file && $(area) && !$(area).html()) {
    $.ajax({
      url: file,
      type: 'GET',
      dataType: 'text',
      success: function (res) {
        $(area).html(res);
        // 刷新代码高亮
        codeTinting.refresh();
      },
      error: function () {
        $(area).html('<div><div class="box ac"><h1 class="ee">404</h1><p>当前页面丢失或无此页面</p></div></div>');
      }
    });
  }
});

// 音乐开关
let music = $('#music');
if (music.length) {
  music = music[0];

  $('body').on('click', '.j_music', function () {
    if (music.paused) {
      music.play();
    } else {
      music.pause();
    }
  });
  let musicIcon = $('.j_music');
  music.addEventListener('progress', function () {
    $(musicIcon).addClass('progress');
  });
  music.addEventListener('canplaythrough', function () {
    $(musicIcon).removeClass('progress');
  });
  music.addEventListener('play', function () {
    $(musicIcon).addClass('playing');
  });
  music.addEventListener('pause', function () {
    $(musicIcon).removeClass('playing');
  });
}

// window.onresize = function () {
// load();
// };
