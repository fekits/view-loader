# @FEKIT/VIEW-LOADER
```$xslt
PROCMS模板开发视图模块化加载器
```

#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)


#### 演示
[https://mcui.asnowsoft.com/plugins/view-loader](https://mcui.asnowsoft.com/plugins/view-loader/)


#### 开始

下载项目: 

```npm
npm i @fekit/view-loader
```

#### 参数
```$xslt
file               {String}    文件路径
```

#### 示例

```javascript
// 引入插件
import viewLoader from '@fekit/view-loader';
viewLoader();
```

#### 版本
```$xslt
v1.0.0 [Latest version]
1. 修复在多级目录下加载模板路径错误的BUG
```

```$xslt
v1.0.0
1. 核心功能完成。
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
