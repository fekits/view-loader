/**
 * MCUI官方普通项目打包工具
 * https://mcui.asnowsoft.com/
 * V1.0.0
 */

// NODE自带的用来获取文件路径的模块插件
const path = require('path');
// 清理文件或文件夹的模块插件
const CleanWebpackPlugin = require('clean-webpack-plugin');
// 处理HTML的模块插件
const HtmlWebpackPlugin = require('html-webpack-plugin');
// 压缩JS的模块插件
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
// 把CSS抽离成独立文件的模块插件
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// 压缩CSS的模块插件
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// 复制文件和文件夹
const CopyWebpackPlugin = require('copy-webpack-plugin');
// 自动压缩图片插件
const TinypngWebpackPlugin = require('tinypng-webpack-plugin');
// 自动压缩图片插件序列号
const tinypngKey = ['nEkBOCe0lyFyd7v3bF6k2MjwLK4UvVX6', 'WBr3Vm6ZqmrsRZaBBm9IXcluqF4CHY3i', 'GvVhRewCjKFNUxpZ34AhSvFJwHHqKfah', 'tm3CLBwcIJ9YJP8F3UE1RJWyD3GxOW2b', '8825ugKrWMSqjwNnos7p94xdJTUWP6hq'];

// 是否开发环境?
const dev = process.env.npm_lifecycle_event === 'serve';
// 是否打包上线？
const babel = process.env.npm_lifecycle_event === 'babel';

const minify = {
  removeComments: true,
  collapseWhitespace: true,
  minifyCSS: true,
  minifyJS: true
};

const config = {
  context: path.resolve(__dirname, './'), // 项目基础路径(要用绝对路径)
  mode: dev ? 'development' : 'production', // 设置开发环境
  devtool: dev ? 'eval-source-map' : 'none', // 是否生成MAP源文件追踪？开如是开发环境则生成，生产环境则不生成
  entry: dev ? { main: './src/assets/js/main.js', mock: './src/assets/js/mock.js' } : { main: './src/assets/js/main' },
  output: {
    // 打包输出的文件名
    filename: dev ? 'js/[name].js' : 'assets/js/[name].[hash:5].min.js',
    // 打包输出路径(要用绝对路经)
    path: path.resolve(__dirname, './dist'),
    publicPath: dev ? '/' : '{{$db.this.host}}/',
    // 打包生成的模块名
    library: '[name]',
    // 打包生成UMD格式
    libraryTarget: 'umd',
    // 设置生成UMD格式时，这个需要设置为"default"
    libraryExport: 'default'
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: dev,
        uglifyOptions: {
          ie8: true,
          output: {
            comments: false,
            beautify: false
          },
          warnings: false
        }
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  plugins: [
    // 清理文件
    new CleanWebpackPlugin(['./dist/']),
    new CopyWebpackPlugin({
      patterns: [
        { from: './README.md', to: '../npm/README.md' }
        // { from: './src/views', to: './views' },
        // { from: './src/img', to: './img' },
        // { from: './src/media', to: './media' },
        // { from: './src/views', to: '../dist/views' }
      ]
    }),
    new MiniCssExtractPlugin({
      filename: dev ? 'assets/css/[name].css' : 'assets/css/[name].[hash:5].min.css'
    }),
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/index.html',
      // 指定输出路径和文件名
      filename: 'index.html',
      inject: true,
      // WEBPACK 编译出现错误
      showErrors: true,
      minify: {
        // 对 HTML 文件进行压缩，minify 的属性值是一个压缩选项或者 false 。默认值为false, 不对生成的 html 文件进行压缩
        // 去除注释
        removeComments: true,
        // 是否去除空格
        collapseWhitespace: true,
        // 是否压缩CSS
        minifyCSS: true,
        // 是否压缩JS
        minifyJS: true
      },
      chunks: dev ? ['mock', 'main'] : ['main']
    }),
    // 头部
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/base/header.htm',
      // 指定输出路径和文件名
      filename: 'views/base/header.htm',
      inject: true,
      showErrors: true,
      minify
    }),
    // 底部
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/base/footer.htm',
      // 指定输出路径和文件名
      filename: 'views/base/footer.htm',
      inject: true,
      showErrors: true,
      minify
    }),
    // 工具
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/base/tools.htm',
      // 指定输出路径和文件名
      filename: 'views/base/tools.htm',
      inject: true,
      showErrors: true,
      minify
    }),
    // 首页
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/home/index.htm',
      // 指定输出路径和文件名
      filename: 'views/home/index.htm',
      inject: true,
      // WEBPACK 编译出现错误
      showErrors: true,
      minify,
      chunks: dev ? ['mock', 'main'] : ['main']
    }),
    // 关于
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/about/index.htm',
      // 指定输出路径和文件名
      filename: 'views/about/index.htm',
      inject: true,
      // WEBPACK 编译出现错误
      showErrors: true,
      minify,
      chunks: dev ? ['mock', 'main'] : ['main']
    }),
    // 首页
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/news/index.htm',
      // 指定输出路径和文件名
      filename: 'views/news/index.htm',
      inject: true,
      // WEBPACK 编译出现错误
      showErrors: true,
      minify,
      chunks: dev ? ['mock', 'main'] : ['main']
    }),
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/news/detail.htm',
      // 指定输出路径和文件名
      filename: 'views/news/detail.htm',
      inject: true,
      // WEBPACK 编译出现错误
      showErrors: true,
      minify,
      chunks: dev ? ['mock', 'main'] : ['main']
    }),
    // 首页
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/product/index.htm',
      // 指定输出路径和文件名
      filename: 'views/product/index.htm',
      inject: true,
      // WEBPACK 编译出现错误
      showErrors: true,
      minify,
      chunks: dev ? ['mock', 'main'] : ['main']
    }),
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/contact/index.htm',
      // 指定输出路径和文件名
      filename: 'views/contact/index.htm',
      inject: true,
      // WEBPACK 编译出现错误
      showErrors: true,
      minify,
      chunks: dev ? ['mock', 'main'] : ['main']
    }),
    new HtmlWebpackPlugin({
      // 处理HTML文件
      template: './src/views/error/index.htm',
      // 指定输出路径和文件名
      filename: 'views/error/index.htm',
      inject: true,
      // WEBPACK 编译出现错误
      showErrors: true,
      minify,
      chunks: dev ? ['mock', 'main'] : ['main']
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: [
              ['@babel/plugin-transform-runtime'],
              [
                // IE8兼容
                '@babel/plugin-transform-modules-commonjs'
              ]
            ]
          }
        }
      },
      {
        test: /\.(html|htm)$/,
        use: {
          loader: 'html-loader',
          options: {
            limit: 1,
            attrs: ['img:src', 'img:data-src', 'audio:src', 'image:src', 'image:xlink:href']
          }
        }
      },
      {
        test: /\.(scss|sass|css)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: dev ? 'style-loader' : MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1,
              name: 'assets/img/[name].[hash:7].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1,
              name: 'assets/media/[name].[hash:7].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(woff2?|eot|ttf|otf|svg)(\?.*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1,
              name: 'assets/font/[name].[hash:7].[ext]'
            }
          }
        ]
      }
    ]
  },
  devServer: {
    hot: true,
    stats: 'errors-only',
    host: '127.0.0.1'
  },
  stats: 'minimal'
};

if (babel) {
  // 上线处理
  config.plugins.push(
    //压缩图片
    new TinypngWebpackPlugin({
      key: tinypngKey
    })
  );
}

module.exports = config;
