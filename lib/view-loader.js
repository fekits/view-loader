// 请求内容
const _ajax = function (param) {
  if (param) {
    const fun = function () {};
    const type = 'GET';
    const url = param.url || '';
    const async = false;
    const dataType = 'text/plain';
    const success = param.success || fun;
    const error = param.error || fun;
    const xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        success(xhr.responseText);
      } else {
        error(xhr);
      }
    };
    xhr.open(type, url, async);
    xhr.send();
  }
};

// 加载内容
const _load = function (file) {
  console.log(file);
  var _code = '';
  _ajax({
    url: file,
    success: function (res) {
      _code = res;
    },
    error: function () {
      _code = '<div><div class="box ac"><h1 class="ee">404</h1><p>当前页面丢失或无此页面</p><p><a class="co-link" href="/">返回首页</a></p></div></div>';
    }
  });
  return _code;
};

// 计算路径
const _path = function (path, file) {
  var aFolder = path.split('/');
  file = file.replace(/[.]{1,2}\//g, function (res) {
    if (res === '../') {
      aFolder.pop();
    }
    return '';
  });
  var rPath = aFolder.join('/');
  var nPath = location.origin + location.pathname + rPath + '/' + file;
  return nPath;
};

// 查找模块
const _find = function (code, path = '') {
  code = code.replace(/(<php\s+(file|link)=['"])([^\n\s'">]*)("><\/php\s*>)/g, function (s1, s2, s3, s4) {
    var newPath = _path(path, s4);
    var subModule = _view(newPath);
    return subModule;
  });
  return code;
};

// 载入视图
const _view = function (file) {
  let code = _load(file);
  const path = file.replace(/\/?\w*\.html?/, '');
  code = _find(code, path);
  return code;
};

const viewLoader = function (file) {
  if (file) {
    return _view(file);
  } else {
    document.documentElement.innerHTML = _find(document.documentElement.innerHTML);
  }
};
module.exports = viewLoader;
